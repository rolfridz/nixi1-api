# Nixi1-api demo

1. Documentación API [POSTMAN Documenter](https://bit.ly/3hj8VZQ).
2. Repositorio GIT [Bitbucket](https://bitbucket.org/rolfridz/nixi1-api/src/master/)
3. Implementación (en Google Kubernetes  Engine) [34.121.75.51](http://34.121.75.51/).

## Iniciar Servidor

Use (se publica en el puerto 8080)
```bash

npm run start

```

### Ejecutar Pruebas

Use (Durante la ejecución de las pruebas se ocupa el puerto 4000)
```bash

npm run test

```

![Screenshot](screenshot.png)

# Tecnologías usadas

1. Node.js + AdonisJs
2. MongoDB (Alojado en [MongoDB Atlas](https://www.mongodb.com/cloud/atlas))
3. Docker
4. Kubernetes (Alojado e implementado en [Google Cloud GKE](https://cloud.google.com/kubernetes-engine))
