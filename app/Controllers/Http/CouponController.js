'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Coupon = use('App/Models/Coupon')

/**
 * Resourceful controller for interacting with coupons
 */
class CouponController {
  /**
   * Show a list of all coupons.
   * GET coupons
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {

    const coupons = await Coupon.all();
    return response.json(coupons)

  }

  /**
   * Display a single task.
   * GET coupons/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show ({ params, request, response }) {
    const coupon = await Coupon.find(params.id);

    if(!coupon){
      return response.status(404).json({data:'the resource does not exist'})
    }

    return response.json(coupon);
  }

  /**
   * Create/save a new coupon.
   * POST coupons
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const couponData = request.all();

    const coupon = await Coupon.create(couponData)

    return response.status(201).json(coupon)
  }

  /**
   * Update coupon details.
   * PUT or PATCH coupons/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {

    const coupon = await Coupon.find(params.id)

    if(!coupon){
      return response.status(404).json({data:'the resource does not exist'})
    }

    const couponData = request.all();

    for (const key in couponData) {
        coupon[key] = couponData[key];
    }

    await coupon.save()
    return response.json(coupon)

  }

  /**
   * Delete a coupon with id.
   * DELETE coupons/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const coupon = await Coupon.find(params.id)

    if(!coupon){
      return response.status(404).json({data:'the resource does not exist'})
    }

    coupon.delete();

    return response.status(204).json(null)

  }
}

module.exports = CouponController
