'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Customer = use('App/Models/Customer')

/**
 * Resourceful controller for interacting with customers
 */
class CustomerController {
  /**
   * Show a list of all customers.
   * GET customers
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {

    const customers = await Customer.all();
    response.json(customers)

  }

  /**
   * Display a single task.
   * GET customers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show ({ params, request, response }) {
    const customer = await Customer.find(params.id);

    if(!customer){
      return response.status(404).json({data:'the resource does not exist'})
    }

    return response.json(customer);
  }

  /**
   * Create/save a new customer.
   * POST customers
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const customerData = request.all();

    const customer = await Customer.create(customerData)

    return response.status(201).json(customer)
  }

  /**
   * Update customer details.
   * PUT or PATCH customers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {

    const customer = await Customer.find(params.id)

    if(!customer){
      return response.status(404).json({data:'the resource does not exist'})
    }

    const customerData = request.all();

    for (const key in customerData) {
        customer[key] = customerData[key];
    }

    await customer.save()
    return response.json(customer)

  }

  /**
   * Delete a customer with id.
   * DELETE customers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const customer = await Customer.find(params.id)

    if(!customer){
      return response.status(404).json({data:'the resource does not exist'})
    }

    customer.delete();

    return response.status(204).json(null)

  }
}

module.exports = CustomerController
