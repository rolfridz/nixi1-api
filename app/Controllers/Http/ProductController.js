'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

const Product = use('App/Models/Product')

/**
 * Resourceful controller for interacting with products
 */
class ProductController {
  /**
   * Show a list of all products.
   * GET products
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {

    const products = await Product.all();
    return response.json(products)

  }

  /**
   * Display a single task.
   * GET products/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show ({ params, request, response }) {
    const product = await Product.find(params.id);

    if(!product){
      return response.status(404).json({data:'the resource does not exist'})
    }

    return response.json(product);
  }

  /**
   * Create/save a new product.
   * POST products
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const productData = request.all();

    const product = await Product.create(productData)

    return response.status(201).json(product)
  }

  /**
   * Update product details.
   * PUT or PATCH products/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {

    const product = await Product.find(params.id)

    if(!product){
      return response.status(404).json({data:'the resource does not exist'})
    }

    const productData = request.all();

    for (const key in productData) {
        product[key] = productData[key];
    }

    await product.save()
    return response.json(product)

  }

  /**
   * Delete a product with id.
   * DELETE products/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const product = await Product.find(params.id)

    if(!product){
      response.status(404).json({data:'the resource does not exist'})
    }

    product.delete();

    return response.status(204).json(null)

  }
}

module.exports = ProductController
