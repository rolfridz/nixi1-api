'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return {
    Documentation: 'https://bit.ly/3hj8VZQ',
    Git: 'https://bit.ly/31mRMcn'
   }
})


Route.group( ()=>{

  Route.get('coupons','CouponController.index')
  Route.get('coupons/:id','CouponController.show')
  Route.post('coupons','CouponController.store')
  Route.put('coupons/:id','CouponController.update')
  Route.delete('coupons/:id','CouponController.destroy')

  Route.get('products','ProductController.index')
  Route.get('products/:id','ProductController.show')
  Route.post('products','ProductController.store')
  Route.put('products/:id','ProductController.update')
  Route.delete('products/:id','ProductController.destroy')

  Route.get('customers','CustomerController.index')
  Route.get('customers/:id','CustomerController.show')
  Route.post('customers','CustomerController.store')
  Route.put('customers/:id','CustomerController.update')
  Route.delete('customers/:id','CustomerController.destroy')


}).prefix('api/v1')
