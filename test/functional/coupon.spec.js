'use strict'

const { test, trait } = use('Test/Suite')('Coupon')
const Coupon = use('App/Models/Coupon')

trait('Test/ApiClient')


test('List Coupons', async ({ client }) => {
  const couponData = {
    title: "coupontestList",
    amount: 20,
    discount_type: "percent",
    description: "Coupon for List",
    date_expires: "2021-01-01",
    usage_limit: 1,
    minimum_amount: 5000,
    maximum_amount : 500000
  }

  await Coupon.create(couponData)

  const response = await client
      .get('/api/v1/coupons')
      .end()

  response.assertStatus(200)
  response.assertJSONSubset([couponData])
})


test('Get Coupon', async ({ client }) => {
  const couponData = {
    title: "coupontestGet",
    amount: 20,
    discount_type: "percent",
    description: "Coupon for Get",
    date_expires: "2021-01-01",
    usage_limit: 1,
    minimum_amount: 5000,
    maximum_amount : 500000
  }

  const coupon = await Coupon.create(couponData)
  const couponID = coupon._id
  const response = await client
      .get(`/api/v1/coupons/${couponID}`)
      .end()

  response.assertStatus(200)
  response.assertJSONSubset(couponData)
})


test('Create Coupon', async ({ client }) => {
  const couponData = {
    title: "coupontestCreate",
    amount: 20,
    discount_type: "percent",
    description: "Coupon for Test Create",
    date_expires: "2021-01-01T00:0:00.000Z",
    usage_limit: 1,
    minimum_amount: 5000,
    maximum_amount : 500000
  }

  const response = await client
      .post('/api/v1/coupons')
      .send(couponData)
      .end()

  response.assertStatus(201)
  response.assertJSONSubset(couponData)
})


test('Update Coupon', async ({ client }) => {
  const couponData = {
    title: "coupontestUpdate",
    amount: 20,
    discount_type: "percent",
    description: "Coupon for Test Update",
    date_expires: "2021-01-01T00:0:00.000Z",
    usage_limit: 1,
    minimum_amount: 5000,
    maximum_amount : 500000
  }


  const coupon = await Coupon.create(couponData)

  couponData.title = "couponUpdateOk"
  couponData.description = "Coupon Update Ok"

  const response = await client
      .put(`/api/v1/coupons/${coupon._id}`)
      .send(couponData)
      .end()

  response.assertStatus(200)
  response.assertJSONSubset(couponData)
})


test('Delete Coupon', async ({ client }) => {
  const couponData = {
    title: "coupontestDelete",
    amount: 20,
    discount_type: "percent",
    description: "Coupon for Test Delete",
    date_expires: "2021-01-01T00:0:00.000Z",
    usage_limit: 1,
    minimum_amount: 5000,
    maximum_amount : 500000
  }
  const coupon = await Coupon.create(couponData)
  const response = await client
      .delete(`/api/v1/coupons/${coupon._id}`)
      .end()

  response.assertStatus(204)
})



