'use strict'

const { test, trait } = use('Test/Suite')('Customer')
const Customer = use('App/Models/Customer')

trait('Test/ApiClient')


test('List Customers', async ({ client }) => {
  const customerData =  {
    first_name: "Rolfrid",
    last_name: "Zambrano",
    age: 26,
    address:
    {
        street_address: "21 2nd Street",
        city: "New York",
        state: "NY",
        postal_code: "10021"
    },
    phone_number:
    [
        {
          type: "home",
          number: "212 555-1234"
        }
    ]
  }

  await Customer.create(customerData)

  const response = await client
      .get('/api/v1/customers')
      .end()

  response.assertStatus(200)
  response.assertJSONSubset([customerData])
})


test('Get Customer', async ({ client }) => {
  const customerData =  {
    first_name: "Roldan",
    last_name: "Arevalo",
    age: 26,
    address:
    {
        street_address: "21 2nd Street",
        city: "New York",
        state: "NY",
        postal_code: "10021"
    },
    phone_number:
    [
        {
          type: "home",
          number: "212 555-1234"
        }
    ]
  }

  const customer = await Customer.create(customerData)
  const customerID = customer._id
  const response = await client
      .get(`/api/v1/customers/${customerID}`)
      .end()

  response.assertStatus(200)
  response.assertJSONSubset(customerData)
})


test('Create Customer', async ({ client }) => {
  const customerData =  {
    first_name: "Pedro",
    last_name: "Perez",
    age: 26,
    address:
    {
        street_address: "21 2nd Street",
        city: "New York",
        state: "NY",
        postal_code: "10021"
    },
    phone_number:
    [
        {
          type: "home",
          number: "212 555-1234"
        }
    ]
  }

  const response = await client
      .post('/api/v1/customers')
      .send(customerData)
      .end()

  response.assertStatus(201)
  response.assertJSONSubset(customerData)
})


test('Update Customer', async ({ client }) => {
  const customerData =  {
    first_name: "Mario",
    last_name: "Bros",
    age: 26,
    address:
    {
        street_address: "21 2nd Street",
        city: "New York",
        state: "NY",
        postal_code: "10021"
    },
    phone_number:
    [
        {
          type: "home",
          number: "212 555-1234"
        }
    ]
  }


  const customer = await Customer.create(customerData)

  customerData.first_name = "Luigi"
  customerData.address.state = "FL"

  const response = await client
      .put(`/api/v1/customers/${customer._id}`)
      .send(customerData)
      .end()

  response.assertStatus(200)
  response.assertJSONSubset(customerData)
})


test('Delete Customer', async ({ client }) => {
  const customerData = {
    first_name: "Mario",
    last_name: "Bros",
    age: 26,
    address:
    {
        street_address: "21 2nd Street",
        city: "New York",
        state: "NY",
        postal_code: "10021"
    },
    phone_number:
    [
        {
          type: "home",
          number: "212 555-1234"
        }
    ]
  }
  const customer = await Customer.create(customerData)
  const response = await client
      .delete(`/api/v1/customers/${customer._id}`)
      .end()

  response.assertStatus(204)
})



