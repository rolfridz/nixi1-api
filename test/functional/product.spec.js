'use strict'

const { test, trait } = use('Test/Suite')('Product')
const Product = use('App/Models/Product')

trait('Test/ApiClient')


test('List Products', async ({ client }) => {
  const productData =  {
    sku: "PGF-RIK",
    name: "Diamonds Are Forever List",
    price: 36,
    retail_price: 36,
    description: "Sociosqu facilisis duis ...",
    category: "Home>Home Decor>Pillows",
    brand: "Vintage Home",
    inventory: 11
}

  await Product.create(productData)

  const response = await client
      .get('/api/v1/products')
      .end()

  response.assertStatus(200)
  response.assertJSONSubset([productData])
})


test('Get Product', async ({ client }) => {
  const productData = {
    sku: "PGF-RIK",
    name: "Diamonds Are Forever Get",
    price: 36,
    retail_price: 36,
    description: "Sociosqu facilisis duis ...",
    category: "Home>Home Decor>Pillows",
    brand: "Vintage Home",
    inventory: 11
}

  const product = await Product.create(productData)
  const productID = product._id
  const response = await client
      .get(`/api/v1/products/${productID}`)
      .end()

  response.assertStatus(200)
  response.assertJSONSubset(productData)
})


test('Create Product', async ({ client }) => {
  const productData = {
    sku: "PGF-RIK",
    name: "Diamonds Are Forever Create",
    price: 36,
    retail_price: 36,
    description: "Sociosqu facilisis duis ...",
    category: "Home>Home Decor>Pillows",
    brand: "Vintage Home",
    inventory: 11
}

  const response = await client
      .post('/api/v1/products')
      .send(productData)
      .end()

  response.assertStatus(201)
  response.assertJSONSubset(productData)
})


test('Update Product', async ({ client }) => {
  const productData = {
    sku: "PGF-RIK",
    name: "Diamonds Are Forever Create",
    price: 36,
    retail_price: 36,
    description: "Sociosqu facilisis duis ...",
    category: "Home>Home Decor>Pillows",
    brand: "Vintage Home",
    inventory: 11
}


  const product = await Product.create(productData)

  productData.name = "productUpdateOk"
  productData.description = "Product Update Ok"

  const response = await client
      .put(`/api/v1/products/${product._id}`)
      .send(productData)
      .end()

  response.assertStatus(200)
  response.assertJSONSubset(productData)
})


test('Delete Product', async ({ client }) => {
  const productData = {
    sku: "PGF-RIK",
    name: "Diamonds Are Forever Delete",
    price: 36,
    retail_price: 36,
    description: "Sociosqu facilisis duis ...",
    category: "Home>Home Decor>Pillows",
    brand: "Vintage Home",
    inventory: 11
}
  const product = await Product.create(productData)
  const response = await client
      .delete(`/api/v1/products/${product._id}`)
      .end()

  response.assertStatus(204)
})



